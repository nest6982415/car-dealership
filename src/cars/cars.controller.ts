import { Body, Controller, Delete, Get, Param, ParseIntPipe, Patch, Post, RequestMapping } from '@nestjs/common';
import { CarsService } from './cars.service';

@Controller('cars')
export class CarsController {

    constructor(
        private readonly carService : CarsService
    ){}

    @Get()
    getAllCars(){
        return this.carService.findAll();
    }

    @Get(':id')
    getCarById( @Param('id', ParseIntPipe) id : number ){
        return this.carService.findOneById(id);
    }

    @Post()
    createCar(@Body() body: any){
        return{
            ok:true,
            method: 'POST'
        }
    }

    @Patch()
    updateCar(@Body() body: any){
        return{
            ok:true,
            method: 'POST'
        }
    }

    @Delete()
    daleteCar(@Param('id' , ParseIntPipe) id: number){
        return id;
    }
}
